# Collection 3.0 channels adaptor API
Channels adaptor
## Version: v1.1.1

**License:**

### /services/channelsadaptor/v1/block

#### POST
##### Summary

createBlock

##### Description

# Request
## Header Parameters
\| Parameter   \| Type  \| Mandatory \| Description  \| Values - Format  \|
\| ----------- \| ----- \| --------- \| ------------ \| ---------------- \|
\| consumerId \| String \| yes \| Unique identification code of the execution  \| [Reglas](https://tdentel.atlassian.net/wiki/download/attachments/1151374/Master%20Headers%20Codes%20eUSB.xlsx?api=v2) \|
\| applicationCode \| String \| no \| app code from consumer \| [Posibles valores](https://tdentel.atlassian.net/wiki/download/attachments/565838019/Master%20Headers%20Codes%20eUSB.xlsx?version=3&modificationDate=1531253559009&cacheVersion=1&api=v2) \|
\| countryCode \| String \| no \| Country Code \| CHL / PER \|
\| requestTimeStamp \| String \| no \| request timestamp \| YYYY-MM-DDThh:mm:ss.sss-Z \|
\| processId \| String \| no \| Message Origin Process \| 20200908NOT0001 \|
\| batchTotalSize \| String \| no \| Total size of records that make up the bulk process \| 100 \|
## Body
\| Parameter   \| Type  \| Mandatory \| Description  \|
\| ----------- \| ----- \| --------- \| ------------ \|
\| createblock \| Object \| yes \| Create Block payload \|
---------------------------------------------------------------------
# Response
## Header Parameters
\| Parameter   \| Type  \| Mandatory \| Description  \| Values - Format  \|
\| ----------- \| ----- \| --------- \| ------------ \| ---------------- \|
\| consumerId \| String \| yes \| Unique identification code of the execution \| [Reglas](https://tdentel.atlassian.net/wiki/download/attachments/1151374/Master%20Headers%20Codes%20eUSB.xlsx?api=v2) \|
\| applicationCode \| String \| no \| app code from consumer \| [Posibles valores](https://tdentel.atlassian.net/wiki/download/attachments/565838019/Master%20Headers%20Codes%20eUSB.xlsx?version=3&modificationDate=1531253559009&cacheVersion=1&api=v2) \|
\| countryCode \| String \| no \| Country Code \| CHL / PER \|
\| requestTimeStamp \| String \| no \| request timestamp \| YYYY-MM-DDThh:mm:ss.sss-Z \|
## Body
\| Parameter   \| Type  \| Mandatory \| Description  \| Values - Format  \|
\| ----------- \| ----- \| --------- \| ------------ \| ---------------- \|
\| Response  \| Object \| yes \| Response object \| ``` { code: string, description: string, responseTimeStamp: '2020-10-29T12:05:38.493Z' } ``` \|

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| createblock | body | createBlock | Yes | [createBlock](#createblock) |
| consumerId | header | Unique identification code of the execution | Yes | string |
| applicationCode | header | app code from consumer | No | string |
| countryCode | header | country code | No | string |
| requestTimeStamp | header | request timestamp | No | string |
| processId | header | processId | No | string |

##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 202 | Accepted | [Response](#response) |
| 400 | Bad Request | [ResponseErrorBadRequest](#responseerrorbadrequest) |
| 404 | Not Found | [ErrorResponse](#errorresponse) |
| 415 | Unsupported Media Type | [ErrorResponseUnsupportMediaType](#errorresponseunsupportmediatype) |
| 500 | Internal Server Error | [ResponseErrorInternalServerError](#responseerrorinternalservererror) |

### /services/channels/v1/customerBillManagement/notification

#### POST
##### Summary

CustomerBillManagementNotification

##### Description

# Request
## Header Parameters
\| Parameter   \| Type  \| Mandatory \| Description  \| Values - Format  \|
\| ----------- \| ----- \| --------- \| ------------ \| ---------------- \|
\| consumerId \| String \| yes \| Unique identification code of the execution  \| [Reglas](https://tdentel.atlassian.net/wiki/download/attachments/1151374/Master%20Headers%20Codes%20eUSB.xlsx?api=v2) \|
\| applicationCode \| String \| no \| app code from consumer \| [Posibles valores](https://tdentel.atlassian.net/wiki/download/attachments/565838019/Master%20Headers%20Codes%20eUSB.xlsx?version=3&modificationDate=1531253559009&cacheVersion=1&api=v2) \|
\| countryCode \| String \| no \| Country Code \| CHL / PER \|
\| requestTimeStamp \| String \| no \| request timestamp \| YYYY-MM-DDThh:mm:ss.sss-Z \|
\| processId \| String \| no \| Message Origin Process \| 20200908NOT0001 \|
## Body
\| Parameter   \| Type  \| Mandatory \| Description  \|
\| ----------- \| ----- \| --------- \| ------------ \|
\| createReactivation \| Object \| yes \| Create Notification payload \|
---------------------------------------------------------------------
# Response
## Header Parameters
\| Parameter   \| Type  \| Mandatory \| Description  \| Values - Format  \|
\| ----------- \| ----- \| --------- \| ------------ \| ---------------- \|
\| consumerId \| String \| yes \| Unique identification code of the execution \| [Reglas](https://tdentel.atlassian.net/wiki/download/attachments/1151374/Master%20Headers%20Codes%20eUSB.xlsx?api=v2) \|
\| applicationCode \| String \| no \| app code from consumer \| [Posibles valores](https://tdentel.atlassian.net/wiki/download/attachments/565838019/Master%20Headers%20Codes%20eUSB.xlsx?version=3&modificationDate=1531253559009&cacheVersion=1&api=v2) \|
\| countryCode \| String \| no \| Country Code \| CHL / PER \|
\| requestTimeStamp \| String \| no \| request timestamp \| YYYY-MM-DDThh:mm:ss.sss-Z \|
## Body
\| Parameter   \| Type  \| Mandatory \| Description  \| Values - Format  \|
\| ----------- \| ----- \| --------- \| ------------ \| ---------------- \|
\| Response  \| Object \| yes \| Response object \| ``` { code: string, description: string, responseTimeStamp: '2020-10-29T12:05:38.493Z' } ``` \|

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| customerBillManagementNotification | body | CustomerBillManagementNotification | Yes | [CustomerBillManagementNotification](#customerbillmanagementnotification) |
| consumerId | header | Unique identification code of the execution | Yes | string |
| applicationCode | header | app code from consumer | No | string |
| countryCode | header | country code | No | string |
| requestTimeStamp | header | request timestamp | No | string |
| processId | header | processId | No | string |

##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 202 | Accepted | [Response](#response) |
| 400 | Bad Request | [ResponseErrorBadRequest](#responseerrorbadrequest) |
| 404 | Not Found | [ErrorResponse](#errorresponse) |
| 415 | Unsupported Media Type | [ErrorResponseUnsupportMediaType](#errorresponseunsupportmediatype) |
| 500 | Internal Server Error | [ResponseErrorInternalServerError](#responseerrorinternalservererror) |

### /services/channelsadaptor/v1/create-reactivation

#### POST
##### Summary

createReactivation

##### Description

# Request
## Header Parameters
\| Parameter   \| Type  \| Mandatory \| Description  \| Values - Format  \|
\| ----------- \| ----- \| --------- \| ------------ \| ---------------- \|
\| consumerId \| String \| yes \| Unique identification code of the execution  \| [Reglas](https://tdentel.atlassian.net/wiki/download/attachments/1151374/Master%20Headers%20Codes%20eUSB.xlsx?api=v2) \|
\| applicationCode \| String \| no \| app code from consumer \| [Posibles valores](https://tdentel.atlassian.net/wiki/download/attachments/565838019/Master%20Headers%20Codes%20eUSB.xlsx?version=3&modificationDate=1531253559009&cacheVersion=1&api=v2) \|
\| countryCode \| String \| no \| Country Code \| CHL / PER \|
\| requestTimeStamp \| String \| no \| request timestamp \| YYYY-MM-DDThh:mm:ss.sss-Z \|
## Body
\| Parameter   \| Type  \| Mandatory \| Description  \|
\| ----------- \| ----- \| --------- \| ------------ \|
\| createReactivation \| Object \| yes \| Create Reactivation payload \|
---------------------------------------------------------------------
# Response
## Header Parameters
\| Parameter   \| Type  \| Mandatory \| Description  \| Values - Format  \|
\| ----------- \| ----- \| --------- \| ------------ \| ---------------- \|
\| consumerId \| String \| yes \| Unique identification code of the execution \| [Reglas](https://tdentel.atlassian.net/wiki/download/attachments/1151374/Master%20Headers%20Codes%20eUSB.xlsx?api=v2) \|
\| applicationCode \| String \| no \| app code from consumer \| [Posibles valores](https://tdentel.atlassian.net/wiki/download/attachments/565838019/Master%20Headers%20Codes%20eUSB.xlsx?version=3&modificationDate=1531253559009&cacheVersion=1&api=v2) \|
\| countryCode \| String \| no \| Country Code \| CHL / PER \|
\| requestTimeStamp \| String \| no \| request timestamp \| YYYY-MM-DDThh:mm:ss.sss-Z \|
## Body
\| Parameter   \| Type  \| Mandatory \| Description  \| Values - Format  \|
\| ----------- \| ----- \| --------- \| ------------ \| ---------------- \|
\| Response  \| Object \| yes \| Response object \| ``` { code: string, description: string, responseTimeStamp: '2020-10-29T12:05:38.493Z' } ``` \|

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| createReactivation | body | createReactivation | Yes | [createReactivation](#createreactivation) |
| consumerId | header | Unique identification code of the execution | Yes | string |
| applicationCode | header | app code from consumer | No | string |
| countryCode | header | country code | No | string |
| requestTimeStamp | header | request timestamp | No | string |

##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 202 | Accepted | [Response](#response) |
| 400 | Bad Request | [ResponseErrorBadRequest](#responseerrorbadrequest) |
| 404 | Not Found | [ErrorResponse](#errorresponse) |
| 415 | Unsupported Media Type | [ErrorResponseUnsupportMediaType](#errorresponseunsupportmediatype) |
| 500 | Internal Server Error | [ResponseErrorInternalServerError](#responseerrorinternalservererror) |

### Models

#### ErrorResponse

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| path | string | _Example:_ `"/services/channelsadaptor/v1/error"` | No |
| status | integer | _Example:_ `404` | No |
| error | string | _Example:_ `"Not Found"` | No |
| message | string | _Example:_ `"Not Found"` | No |
| timeStamp | dateTime |  | No |

#### ErrorResponseUnsupportMediaType

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| title | string | _Example:_ `"Unsupported Media Type"` | No |
| status | integer | _Example:_ `415` | No |
| detail | string | _Example:_ `"Content type ' not supported"` | No |
| path | string | _Example:_ `"/services/channelsadaptor/v1/block"` | No |
| message | string | _Example:_ `"error.http.415"` | No |

#### ResponseErrorBadRequest

response

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| code | string | result code<br>_Example:_ `"6"` | No |
| description | string | result description<br>_Example:_ `"The transactionId is duplicated"` | No |
| responseTimeStamp | dateTime | timestamp de resultado | No |

#### ResponseErrorInternalServerError

response

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| code | string | result code<br>_Example:_ `"99"` | No |
| description | string | result description<br>_Example:_ `"Internal Server Error"` | No |
| responseTimeStamp | dateTime | timestamp de resultado | No |

#### Response

response

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| code | string | result code<br>_Example:_ `"0"` | No |
| description | string | result description<br>_Example:_ `"successful execution"` | No |
| responseTimeStamp | dateTime | timestamp de resultado | No |

#### createBlock

create Block

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| customerOrder | [CustomerOrder](#customerorder) |  | Yes |

#### CustomerOrder

customer order

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| area | string | Area<br>_Example:_ `"Block"` | Yes |
| subArea | string | Sub Area<br>_Example:_ `"Movil - Postpago"` | Yes |
| orderType | string | Order Type<br>_Example:_ `"Orden"` | Yes |
| customerAccount | [CustomerAccount](#customeraccount) |  | Yes |
| salesChannel | [SalesChannel](#saleschannel) |  | No |
| customerOrderItem | [CustomerOrderItem](#customerorderitem) |  | No |

#### CustomerAccount

customer account

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| individualIdentification | [IndividualIdentification](#individualidentification) |  | Yes |
| billingAccount | [BillingAccount](#billingaccount) |  | Yes |
| asset | [Asset](#asset) |  | No |

#### IndividualIdentification

individual identification

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| number | string | Number of identification: ex: 24156862-8<br>_Example:_ `"15633459-6"` | Yes |
| type | string | Type of identification: ex: RUT<br>_Example:_ `"RUT"` | Yes |

#### BillingAccount

billing account

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| externalId | string | Identification of Billing Account in BSCS<br>_Example:_ `"1.0123863871623"` | Yes |

#### Asset

Asset

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| line | [Line](#line) |  | No |

#### Line

Line

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| msisdn | string | MSISDN<br>_Example:_ `56999999999` | Yes |

#### SalesChannel

sales channel

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| createBy | string | Identify of channel requester<br>_Example:_ `"SAP"` | Yes |
| orderCommercialChannel | string | Code of channel: ex: WEB, IVR, ...<br>_Example:_ `"IVR"` | Yes |

#### CustomerOrderItem

customer order item

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| transactionDetails | [TransactionDetails](#transactiondetails) |  | No |
| notification | [Notification](#notification) |  | No |

#### TransactionDetails

Transaction Details

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| reason | string | reason<br>_Example:_ `"S2"` | Yes |
| reasonDescription | string | reason description<br>_Example:_ `"Suspensión No pago-unidirecional"` | No |

#### Notification

notification

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| notificationId | string | notification id<br>_Example:_ `"NOT_0001"` | No |
| channel | string | channel<br>_Example:_ `"CIS"` | No |
| deliveryMethod | string | delivery method<br>_Example:_ `"SMS"` | No |

#### createReactivation

create reactivation

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| customerOrder | [CustomerOrder](#customerorder) |  | Yes |

#### Equipment

Equipment detail

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| imei | string | IMEI<br>_Example:_ `"1.98123717329817293812793"` | Yes |
| brandName | string | Equipment brand name<br>_Example:_ `"Alcatel"` | No |
| modelNumber | string | Equipment model number<br>_Example:_ `"Pixi 4 4,0"` | No |

#### CustomerBillManagementNotification

create notification

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| notification | [Notification](#notification) |  | Yes |

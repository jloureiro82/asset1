# Channels adaptor Configuration



| Name | Description | Type | Default value
-----| ----------- | ---- | ------------- |
logging.level.ROOT| Define the logging level for logs from ROOT | string | INFO |
logging.level.io.github.jhipster| Define the logging level for logs from jHipster | string  | INFO |
logging.level.com.readinessit.adaptors| Define the logging level for logs for the adaptor | string  | INFO |
app.service.configs.use-cache | Switch on/off to use cache | boolean | false
app.endpoints.routes.NOTIFICATION.REST.url | Rest url for notifications | string | 
app.endpoints.routes.NOTIFICATION.REST.transactionId | Rest Transaction id for notification | string | 
app.endpoints.routes.NOTIFICATION.SOAP.url | SOAP url for notifications | string | 
app.endpoints.routes.NOTIFICATION.SOAP.transactionId | SOAP Transaction id for notification | string | 
app.endpoints.routes.REACTIVATION.REST.url | SOAP url for reactivation | string | 
app.endpoints.routes.REACTIVATION.REST.transactionId | SOAP Transaction id for reactivation | string | 
app.endpoints.routes.REACTIVATION.SOAP.url | SOAP url for reactivation | string | 
app.endpoints.routes.REACTIVATION.SOAP.transactionId | SOAP Transaction id for reactivation | string | 
app.endpoints.routes.BLOCK.REST.url | SOAP url for block | string | 
app.endpoints.routes.BLOCK.REST.transactionId | SOAP Transaction id for block | string | 
app.endpoints.routes.BLOCK.SOAP.url | SOAP url for block | string | 
app.endpoints.routes.BLOCK.SOAP.transactionId | SOAP Transaction id for block | string | 
app.housekeeping.offSetSeconds | Offset for request timestamp | string | 
restTemplate.logging.intercept.enabled | Enable or disable inbound and outbound logging for rest calls | boolean | true
restTemplate.factory.readTimeout | Timeout value for reading http calls | Long | 5000
restTemplate.factory.connectTimeout | Timeout value for connecting to server for http calls | Long | 3000
restTemplate.httpClient.maxConnTotal | Total maximum connections available | Long | 100
restTemplate.httpClient.maxConnPerRoute | Maximum connections per rout | Long | 10
